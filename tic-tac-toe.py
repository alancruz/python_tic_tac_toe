import re

charizard = """
                 ."-,.__
                 `.     `.  ,
              .--'  .._,'"-' `.
             .    .'         `'
             `.   /          ,'
               `  '--.   ,-"'
                `"`   |  \\
                   -. \, |
                    `--Y.'      ___.
                         \     L._, \\
               _.,        `.   <  <\                _
             ,' '           `, `.   | \            ( `
          ../, `.            `  |    .\`.           \ \_
         ,' ,..  .           _.,'    ||\|            )  '".
        , ,'   \           ,'.-.`-._,'  |           .  _._`.
      ,' /      \ \        `' ' `--/   | \          / /   ..\\
    .'  /        \ .         |\__ - _ ,'` `        / /     `.`.
    |  '          ..         `-...-"  |  `-'      / /        . `.
    | /           |L__           |    |          / /          `. `.
   , /            .   .          |    |         / /             ` `
  / /          ,. ,`._ `-_       |    |  _   ,-' /               ` \\
 / .           \\"`_/. `-_ \_,.  ,'    +-' `-'  _,        ..,-.    \`.
.  '         .-f    ,'   `    '.       \__.---'     _   .'   '     \ \\
' /          `.'    l     .' /          \..      ,_|/   `.  ,'`     L`
|'      _.-""` `.    \ _,'  `            \ `.___`.'"`-.  , |   |    | \\
||    ,'      `. `.   '       _,...._        `  |    `/ '  |   '     .|
||  ,'          `. ;.,.---' ,'       `.   `.. `-'  .-' /_ .'    ;_   ||
|| '              V      / /           `   | `   ,'   ,' '.    !  `. ||
||/            _,-------7 '              . |  `-'    l         /    `||
. |          ,' .-   ,' ||               | .-.        `.      .'     ||
 `'        ,'    `".'    |               |    `.        '. -.'       `'
          /      ,'      |               |,'    \-.._,.'/'
          .     /        .               .       \    .''
        .`.    |         `.             /         :_,'.'
          \ `...\   _     ,'-.        .'         /_.-'
           `-.__ `,  `'   .  _.>----''.  _  __  /
                .'        /"'          |  "'   '_
               /_|.-'\ ,".             '.'`__'-( \\
                 / ,"'"\,'               `/  `-.|"
"""

explanation = """
The object of Tic Tac Toe is to get three in a row.
You play on a three by three game board.

This is a two players game only!

You must select a number from 1 to 9 that represents
the position of the board where you want to put your marker.
The next figure is a representation of the positions in
the game board"""

def print_game_board():
    print("   |   |")
    print("_7_|_8_|_9_")
    print("   |   |")
    print("_4_|_5_|_6_")
    print("   |   |")
    print(" 1 | 2 | 3")

print("{:*^80}".format("Tic-Tac-Toe Game!"))
print(explanation)
print_game_board()
print("\n\n")

p1_marker = ""

while p1_marker.upper() != 'X' and p1_marker.upper() != 'O':
    p1_marker = input("Player one select your marker. Enter 'X' or 'O': ").upper()
    if p1_marker != 'X' and p1_marker != 'O':
        print("\nError: User input value is not 'x' or 'o'\n")

p2_marker = 'O' if p1_marker == 'X' else 'X'
board = ["_" for x in range(0,9)]
winner = False

def check_if_player_wins(marker):
    return board[6] == board[7] == board[8] == marker\
    or board[3] == board[4] == board[5] == marker\
    or board[0] == board[1] == board[2] == marker\
    or board[0] == board[3] == board[6] == marker\
    or board[1] == board[4] == board[7] == marker\
    or board[2] == board[5] == board[8] == marker\
    or board[0] == board[4] == board[8] == marker\
    or board[6] == board[4] == board[2] == marker\

def current_board():
    print("   |   |    ")
    print("_{}_|_{}_|_{}_".format(board[6], board[7], board[8]))
    print("   |   |    ")
    print("_{}_|_{}_|_{}_".format(board[3], board[4], board[5]))
    print("   |   |    ")
    print(" {} | {} | {} ".format(board[0], board[1], board[2]))
    print("\n \n")

print("*************************************************")
print("Player one will use the '%(player_1_marker)s' mark" % { 'player_1_marker': p1_marker.upper() })
print("Player two will use the '%(player_2_marker)s' mark" % { 'player_2_marker': p2_marker.upper() })
print("*************************************************")
print("\n \n")

def run_turn(marker):
    current_board()
    position = ""
    while position not in range(0, len(board)):
        position = input("Select a board position to place your marker(1-9): ")

        if re.match("\D", position) or position == "":
            print("Please enter a numeric digit in the range of 1-9")
            continue
        else:
            position = int(position) - 1

        while position not in range(0, len(board)):
            print("That position is not in the board")
            position = input("Select a board position to place your marker(1-9): ")

            if re.match("\D", position):
                print("Please enter a numeric digit in te range of 1-9")
                continue
            else:
                position = int(position) - 1

    while board[position] != "_":
        print("That position already have a marker\n")
        position = input("Select another board position to place your marker(1-9): ")

        if re.match("\D", position):
            print("Please enter a numeric digit in te range of 1-9")
            continue
        else:
            position = int(position) - 1

    board[position] = marker
    return check_if_player_wins(marker)

while "_" in board and winner != True:
    print("\n\n**********Player one turn.**********\n\n")
    winner = run_turn(p1_marker)
    if winner:
        print("|||||||||----------------Player one wins!.----------------|||||||||")
        print(charizard)
        print("|||||||||----------------Player one wins!.----------------|||||||||\n")
        current_board()
        break
    elif "_" in board:
        print("\n\n**********Player two turn.**********\n\n")
        winner = run_turn(p2_marker)
        if winner:
            print("|||||||||----------------Player two wins!.----------------|||||||||")
            print(charizard)
            print("|||||||||----------------Player two wins!.----------------|||||||||\n")
            current_board()
            break
    else:
        print("it's a tie")
        current_board()
#¿Se podrá hacer un juego de terminal por internet?
